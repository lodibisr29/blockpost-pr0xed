// Get the current URL
var url = window.location.href;

// Check if the URL is loaded through a web proxy
if (url.indexOf('://ikatchelo.github.io/blockpost/') === -1) {
  // The website is not loaded through a web proxy, so show a message
  document.body.innerHTML = 'Please load this page through a web proxy to access the content.';
  return;
}

// The website is loaded through a web proxy, so allow it to load
document.body.style.display = 'block';
